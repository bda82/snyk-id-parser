import re
import time
import datetime
from enum import Enum

class Configuration(Enum):
    CGID_DEFAULT_UNDEFINED = 'cgid:undefined:'
    CGID_DEFAULT_START = 'CGID:'
    CGID_SEPARATOR = ':'
    CGID_MAX_DIGITS = 12


class VulnerabilitySources(Enum):
    CVE = 'C'
    NPM = 'N'
    SNYK = 'S'
    USER = 'U'
    MS = 'MS'


class Timezone:
    def now(self):
        return datetime.datetime.now()


timezone = Timezone()


class LASTID:

    @property
    def cgid(self):
        return str(int(time.time()))


class CGIDService:

    def __init__(self):
        self.vulnerability_source_dispatch_tags = {
            VulnerabilitySources.SNYK.value: self.__generate_cgid_snyk,
            VulnerabilitySources.CVE.value: self.__generate_cgid_cve,
            VulnerabilitySources.NPM.value: self.__generate_cgid_npm,
            VulnerabilitySources.MS.value: self.__generate_cgid_ms,
            VulnerabilitySources.USER.value: self.__generate_cgid_user,
        }

    @staticmethod
    def __years(current_year):
        y = str(current_year)
        if len(y) == 4:
            return y[2:]
        else:
            y = str(timezone.now().year)
            return y[2:]

    @staticmethod
    def __only_digits(string):
        sis = re.sub(r"\D", '', string)
        return sis

    @staticmethod
    def __create_id_numbers_set(numbers):
        numbers_length = len(numbers)
        numbers_length_as_string = str(numbers_length)

        zeros = '0' * (Configuration.CGID_MAX_DIGITS.value - numbers_length)

        if numbers_length < 10:
            numbers_length_as_string = '0' + numbers_length_as_string

        return ''.join([numbers_length_as_string, numbers, zeros])

    @property
    def __current_year(self):
        return str(timezone.now().year)

    def generate_new_cgid(self, original_id, vulnerability_source):
        try:
            return self.vulnerability_source_dispatch_tags[vulnerability_source](original_id, vulnerability_source)
        except Exception as err:
            return self.__not_found(vulnerability_source)

    @staticmethod
    def __not_found(vulnerability_source: str):
        print(f'Vulnerability source code "{vulnerability_source}" not found')
        return None

    def __generate_cgid_snyk(self, original_id, vulnerability_source):
        # SNYK-JS-JSON8MERGEPATCH-1038399
        # SNYK-JAVA-ORGWEBJARSBOWER-1038258
        # SNYK-JAVA-ORGJENKINSCIPLUGINS-1037283
        snyk_as_list = original_id.split("-")
        snyk_digits = self.__only_digits(snyk_as_list[-1])
        current_year = self.__years(self.__current_year)
        snyk_short_year_and_numbers = current_year + snyk_digits
        snyk_numbers_set = self.__create_id_numbers_set(snyk_short_year_and_numbers)
        return Configuration.CGID_SEPARATOR.value.join([
                Configuration.CGID_DEFAULT_START.value,
                self.__current_year,
                VulnerabilitySources.SNYK.value,
                snyk_numbers_set
            ])

    def __generate_cgid_cve(self, original_id, vulnerability_source):
        original_id_as_list = original_id.split('-')

        if len(original_id_as_list) == 3:
            cve_year = self.__years(original_id_as_list[1])
            cve_numbers = original_id_as_list[2]
            cve_short_year_and_numbers = cve_year + cve_numbers
            cve_numbers_set = self.__create_id_numbers_set(cve_short_year_and_numbers)
            return Configuration.CGID_SEPARATOR.value.join([
                Configuration.CGID_DEFAULT_START.value,
                self.__current_year,
                VulnerabilitySources.CVE.value,
                cve_numbers_set
            ])
        return ''

    def __generate_cgid_npm(self, original_id, vulnerability_source):
        npm_year = self.__years(self.__current_year)
        npm_numbers = self.__only_digits(original_id)
        npm_short_year_and_numbers = npm_year + npm_numbers
        set_of_npm_numbers = self.__create_id_numbers_set(npm_short_year_and_numbers)
        return Configuration.CGID_SEPARATOR.value.join([
            Configuration.CGID_DEFAULT_START.value,
            self.__current_year,
            VulnerabilitySources.NPM.value,
            set_of_npm_numbers
        ])

    def __generate_cgid_ms(self, original_id, vulnerability_source):
        ms_year = self.__years(self.__current_year)
        ms_numbers = str(int(self.__only_digits(original_id)) + 1)
        ms_short_year_and_numbers = ms_year + ms_numbers
        set_of_ms_numbers = self.__create_id_numbers_set(ms_short_year_and_numbers)
        return Configuration.CGID_SEPARATOR.value.join([
            Configuration.CGID_DEFAULT_START.value,
            self.__current_year,
            VulnerabilitySources.MS.value,
            set_of_ms_numbers
        ])

    def __generate_cgid_user(self, original_id, vulnerability_source):
        user__year = self.__years(self.__current_year)
        last_cgid_element = self.__get_last_sync_cgid()
        last_ID = last_cgid_element.cgid
        user_numbers = self.__only_digits(last_ID)
        user_short_year_and_numbers = user__year + user_numbers
        set_of_user_numbers = self.__create_id_numbers_set(user_short_year_and_numbers)
        return Configuration.CGID_SEPARATOR.value.join([
            Configuration.CGID_DEFAULT_START.value,
            self.__current_year,
            VulnerabilitySources.USER.value,
            set_of_user_numbers
        ])

    def __get_last_sync_cgid(self):
        return LASTID.cgid


c = CGIDService()

original_id, vulnerability_source = 'CVE-2019-111002', VulnerabilitySources.CVE.value
i = c.generate_new_cgid(original_id, vulnerability_source)
print(i)

original_id, vulnerability_source = 'npm:1570', VulnerabilitySources.NPM.value
i = c.generate_new_cgid(original_id, vulnerability_source)
print(i)

original_id, vulnerability_source = 'SNYK-JS-JSON8MERGEPATCH-1038399', VulnerabilitySources.SNYK.value
i = c.generate_new_cgid(original_id, vulnerability_source)
print(i)

original_id, vulnerability_source = 'SNYK-JAVA-ORGWEBJARSBOWER-1038258', VulnerabilitySources.SNYK.value
i = c.generate_new_cgid(original_id, vulnerability_source)
print(i)

original_id, vulnerability_source = 'SNYK-JS-JSON8MERGEPATCH-1038399', VulnerabilitySources.SNYK.value
i = c.generate_new_cgid(original_id, vulnerability_source)
print(i)